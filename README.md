# mohsen_et_al_scripts

repository containing available scripts for the analyses presented in Mohsen et al. "Autoregulation of the Reduced Complexity gene by low-affinity binding modulates cytokinin action to shape leaf diversity" 